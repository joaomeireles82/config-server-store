package com.kentratech.authorizationserver.services;

import com.kentratech.authorizationserver.entities.User;
import com.kentratech.authorizationserver.repositories.UserRepository;
import com.kentratech.authorizationserver.utils.Random;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    EmailService emailService;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    Random random;

}
