package com.kentratech.authorizationserver.config;


import com.kentratech.authorizationserver.entities.Client;
import com.kentratech.authorizationserver.entities.User;
import com.kentratech.authorizationserver.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CustomTokenEnhancer implements TokenEnhancer {

    @Autowired
    ClientDetailsService clientDetailsService;

    @Autowired
    ClientRepository clientRepository;


    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {

        Client c =  clientRepository.findByClientId(oAuth2Authentication.getOAuth2Request().getClientId());

        Map<String, Object> additionalInfo = new HashMap<>();

        additionalInfo.put("user_id", "1234324");
        additionalInfo.put("aud", "oauth2-resource");
        additionalInfo.put("tenants", c.getTenants());

        DefaultOAuth2AccessToken defaultOAuth2AccessToken = (DefaultOAuth2AccessToken) oAuth2AccessToken;

        defaultOAuth2AccessToken.setAdditionalInformation(additionalInfo);

        return oAuth2AccessToken;
    }
}
