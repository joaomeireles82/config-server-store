package com.kentratech.authorizationserver.repositories;

import com.kentratech.authorizationserver.entities.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, String> {

    Client  findByClientId(String clientId);
}
