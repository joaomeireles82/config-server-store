package com.kentratech.authorizationserver.services;

import com.kentratech.authorizationserver.entities.Client;
import com.kentratech.authorizationserver.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    public Client getClientById(String clientId){

        return clientRepository.findByClientId(clientId);

    }

}
