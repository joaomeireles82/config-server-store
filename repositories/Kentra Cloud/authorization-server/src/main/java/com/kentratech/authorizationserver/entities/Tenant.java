package com.kentratech.authorizationserver.entities;

import javax.persistence.*;

@Entity
@Table(name = "tenants")
public class Tenant {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tenants_id_seq")
    private Integer id;

    @Column(name="code")
    private String code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
