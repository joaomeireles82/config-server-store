package com.kentratech.authorizationserver.utils;

import org.springframework.context.annotation.Configuration;

@Configuration
public class Random {

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public String randomCode(Integer size){

        String SALTCHARS = ALPHA_NUMERIC_STRING;
        StringBuilder salt = new StringBuilder();
        java.util.Random rnd = new java.util.Random();
        while (salt.length() < size) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }

        return salt.toString();
    }

}
