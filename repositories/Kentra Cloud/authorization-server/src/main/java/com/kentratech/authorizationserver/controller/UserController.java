package com.kentratech.authorizationserver.controller;

import com.kentratech.authorizationserver.dto.UserDto;
import com.kentratech.authorizationserver.entities.User;
import com.kentratech.authorizationserver.repositories.UserRepository;
import com.kentratech.authorizationserver.services.EmailService;
import com.kentratech.authorizationserver.utils.Random;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    EmailService emailService;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    Random random;


    @ApiOperation(value = "send a user code (business model) to validate that it corresponds to a specific user. Should indicate if you want to receive notification (email)")
    @RequestMapping(value = "/validate", method = RequestMethod.GET)
    public ResponseEntity<JSONObject> validateUser(@RequestParam("userCode") String userCode, @RequestParam(value = "notification", defaultValue = "true") boolean notification, @RequestParam(value = "notificationType", defaultValue = "email") String notificationType){

        Optional<User> user =  userRepository.findByCode(userCode);

        if (user.isPresent()){
            String randomCode = random.randomCode(18);
            user.get().setVerificationCode(randomCode);
            userRepository.save(user.get());
            emailService.sendMail(randomCode);
            return ResponseEntity.ok().body(new JSONObject("{'validate':'true'}"));
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JSONObject("{'validate':'true'}"));
        }

    }

    @ApiOperation(value = "Get all users")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok(userRepository.findAll());
    }

    @ApiOperation(value = "Creating new user")
    @PostMapping("/users")
    public User createUser(@RequestBody UserDto userDto) {

        User user = modelMapper.map(userDto, User.class);
        userRepository.save(user);
        return user;
    }

    @ApiOperation(value = "Get user information")
    @GetMapping(value = "{id}")
    public ResponseEntity<User> validateUser(@PathVariable Long userId){

        Optional<User> user =  userRepository.findById(userId);

        if (user.isPresent()){
            userRepository.save(user.get());
            return ResponseEntity.ok(user.get());
        }
        else {
            return ResponseEntity.notFound().build();
        }

    }

    @ApiOperation(value = "Edit information exists user")
    @PutMapping(value = "{id}")
    public User editUser(@PathVariable Long userId) {

        User user = modelMapper.map(userId, User.class);
        userRepository.save(user);
        return user;
    }

    @ApiOperation(value = "delete user")
    @DeleteMapping(value = "{id}")
    public ResponseEntity<Long> deleteUser(@PathVariable Long userId) {

        Optional<User> user =  userRepository.findById(userId);

        if (user.isPresent()){
            userRepository.deleteById(userId);
            return ResponseEntity.ok(userId);
        }
        else{
            return ResponseEntity.notFound().build();

        }
    }


    @ApiOperation(value = "Verify a verification code")
    @RequestMapping(value = "/verification_code", method = RequestMethod.GET)
    public ResponseEntity<JSONObject> validateVerificationCode(@RequestParam("userCode") String userCode, @RequestParam("verificationCode") String verificationCode){

        Optional<User> user = userRepository.findByCode(userCode);

        if (user.isPresent() && user.get().getVerificationCode().equals(verificationCode.trim())) {
            return ResponseEntity.ok().body(new JSONObject("{'validate':'true'}"));

        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JSONObject("{'validate':'false'}"));
        }
    }

    @ApiOperation(value = "Edit or create new password")
    @PostMapping(value = "/change_password")
    public ResponseEntity<JSONObject> setPassword(@RequestBody UserDto userDto){

        User user = modelMapper.map(userDto, User.class);
        Optional<User> userOptional =  userRepository.findByCode(user.getCode());

        if (userOptional.isPresent()) {

            int strength = 12;
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(strength, new SecureRandom());
            String encodedPassword = bCryptPasswordEncoder.encode(userDto.getPassword());
            userOptional.get().setPassword(encodedPassword);
            userRepository.save(userOptional.get());

            return ResponseEntity.ok().body(new JSONObject("{'validate':'true'}"));
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JSONObject("{'validate':'false'}"));
        }


    }
}
