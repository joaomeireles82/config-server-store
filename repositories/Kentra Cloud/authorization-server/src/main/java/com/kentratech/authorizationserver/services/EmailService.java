package com.kentratech.authorizationserver.services;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Async
public class EmailService {

    private JavaMailSender javaMailSender;

    public EmailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMail(String message) {

        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setTo("joao.meireles@kentratech.com","helder.nunes@kentratech.com");
        mailMessage.setSubject("Envio de código para registo na Aplicação");
        mailMessage.setText(message);

        mailMessage.setFrom("suporte@kentratech.com");

        javaMailSender.send(mailMessage);
    }

}
