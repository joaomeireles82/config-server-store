package com.kentratech.authorizationserver.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.*;

public class AuthClientDetail extends Client implements ClientDetails {

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return null;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return null;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {

        return null;
    }

    @Override
    public Set<String> getResourceIds() {
        return null;
    }

    @Override
    public Set<String> getScope() {
        return null;
    }

    @Override
    public boolean isSecretRequired() {
        return false;
    }

    @Override
    public boolean isScoped() {
        return false;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        String[] values = super.getWebServerRedirectUri().split(",");
        Set<String> list = new HashSet<>(Arrays.asList(values));
        return list;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return super.getAccessTokenValidity();
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return super.getRefreshTokenValidity();
    }

    @Override
    public boolean isAutoApprove(String s) {
        return false;
    }
}
