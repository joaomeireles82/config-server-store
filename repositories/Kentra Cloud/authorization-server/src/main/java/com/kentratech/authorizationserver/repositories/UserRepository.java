package com.kentratech.authorizationserver.repositories;

import com.kentratech.authorizationserver.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);
    Optional<User> findByCode(String username);
    Optional<User> findById(Integer id);
    User save(User user);

}
